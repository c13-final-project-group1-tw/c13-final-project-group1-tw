### Git Init
- ### Create a new repository
```Bash
git clone git@gitlab.com:c13-final-project-group1-tw/c13-final-project-group1-tw.git
cd c13-final-project-group1-tw
touch README.md
git add README.md
git commit -m "add README"
git push -u origin master
```
### Push an existing folder (main, remote .git file)(if remote is fail, use "clone" on upper)
```Bash
cd existing_folder
git init
git remote add origin git@gitlab.com:c13-final-project-group1-tw/c13-final-project-group1-tw.git
git add .
git commit -m "Initial commit"
git push -u origin master
```
- Push an existing Git repository
```Bash
cd existing_repo
git remote rename origin old-origin
git remote add origin git@gitlab.com:c13-final-project-group1-tw/c13-final-project-group1-tw.git
git push -u origin --all
git push -u origin --tags
```
### ---------------------------------------------------------------
### Postgresql start setting
- **Windows**
```Bash
sudo service postgresql start
```
- **Mac**
```Bash

```
- **sudo create user database**
```Bash
sudo su postgres
sudo -U <username> -W -h <hostname> <DBname>
```
### --------------------------------------------------------------
### Yarn
- **Install yarn at global by npm**
```Bash
sudo npm install -g yarn
```
### Yarn add libraries
- **BAD001 test case, jest install**
```Bash
yarn add --dev ts-node typescript @types/node
yarn add --dev jest
yarn add --dev typescript ts-jest @types/jest
yarn ts-jest config:init
```
- **BAD003 knex install**
```Bash
yarn add knex @types/knex pg @types/pg
```
|- **knexfile.ts**
```Bash
yarn knex init -x ts
```
|- **environment change**
```Bash
knex --env production <any-command>
```