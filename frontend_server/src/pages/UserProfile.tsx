import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { getUserThunk } from '../redux/auth/thunk';
import { useForm, Controller } from "react-hook-form";
import modules from '../css/RegisterPage.module.css';
import { Button, Form, Container } from 'react-bootstrap';
import { IRootState } from '../redux/store';
import { push } from 'connected-react-router';

interface IUsersProfileProps {
    id: number;
    users_is_active: boolean;
    seller_is_active: boolean;
    customer_first_name: string;
    customer_last_name: string;
    username: string;
    email: string;
    location: string;
    gender: string;
    is_active: boolean;
    photo_str: number;
}

export interface IPutData {
    id: number;
    users_is_active: boolean;
    seller_is_active: boolean;
    customer_first_name: string;
    customer_last_name: string;
    password:string;
    username: string;
    email: string;
    location: string;
    gender: string;
    is_active: boolean;
    photo_str: number;
}

function UserProfile() {
    const dispatch = useDispatch();
    const userResultList:IUsersProfileProps | null = useSelector((state:IRootState)=> state.auth.userResultList) || null;

    useEffect(() =>{
        dispatch(getUserThunk());
    }, [dispatch]);

    useEffect(()=>{

    })

    
    console.log(userResultList);
    const { control, handleSubmit, formState: {errors} } = useForm<IPutData>();
    const onSubmit = (data: IPutData) => {
        alert('update success!')
        dispatch(push('/home'));
    };
    return (
        <div>
                    <Container className={modules.container}>
                    <h1>Profile</h1>
                        <Form onSubmit={handleSubmit(onSubmit)}>
                            
                                <Form.Group controlId="formFirstName">
                                <Form.Label>First Name | Last Name</Form.Label>
                                            <Controller
                                            name="customer_first_name"
                                            control={control}
                                            defaultValue={userResultList?.customer_first_name}
                                            rules={{ required: true }}
                                            render={({ field }) => <Form.Control {...field} 
                                            placeholder="Enter Your first name" required/>}
                                            />
                                        
                                    </Form.Group>
                                    <Form.Group controlId="formLastName">
                                            <Controller
                                            name="customer_last_name"
                                            control={control}
                                            defaultValue={userResultList?.customer_last_name}
                                            rules={{ required: true }}
                                            render={({ field }) => <Form.Control {...field} 
                                            placeholder="Enter Your last name" required/>}
                                            />
                                    </Form.Group>
        
                            <Form.Group controlId="formRegPassword">
                                <Form.Label>Password</Form.Label>
                                <Controller
                                    name="password"
                                    control={control}
                                    defaultValue=""
                                    rules={{ required: true }}
                                    
                                    render={({ field }) => <Form.Control {...field} 
                                    placeholder="Enter Your Password" type="password" required />}
                                    />                    
                            </Form.Group>

                            <Form.Group controlId="formUsername">
                                <Form.Label>Username</Form.Label>
                                <Controller
                                    name="username"
                                    control={control}
                                    defaultValue={userResultList?.username}
                                    rules={{ required: true }}
                                    render={({ field }) => <Form.Control {...field} 
                                    placeholder="Username show in website" required/>}
                                    />
                            </Form.Group>

                            <Form.Group controlId="formRegEmail">
                                <Form.Label>E-mail</Form.Label>
                                <Controller
                                    name="email"
                                    control={control}
                                    defaultValue={userResultList?.email}
                                    rules={{ required: true }}
                                    render={({ field }) => <Form.Control {...field} 
                                    placeholder="Enter Your E-mail" type="email" required/>}
                                    />
                            </Form.Group>

                            <Form.Group controlId="formLocation">
                                <Form.Label>Location</Form.Label>
                                <Controller
                                    name="location"
                                    control={control}
                                    defaultValue={userResultList?.location}
                                    rules={{ required: true }}
                                    render={({ field }) => <Form.Control {...field} 
                                    placeholder="Enter Your Location" />}
                                    />
                            </Form.Group>

                            <Form.Group controlId="formGender">
                                <Form.Label>Gender</Form.Label>
                                <Controller
                                    name="gender"
                                    control={control}
                                    defaultValue={userResultList?.gender}
                                    rules={{ required: true }}
                                    render={({ field }) => <Form.Control {...field} as="select"
                                    defaultValue={userResultList?.gender} required>
                                        <option>secert</option>
                                        <option>M</option>
                                        <option>F</option>
                                    </Form.Control>}
                                    />
                            </Form.Group>

                            <Button variant="primary" type="submit">
                                Update
                            </Button>
                        </Form>
                    </Container>
                    {errors && errors.password}
            
        </div>
    )
}

export default UserProfile;
