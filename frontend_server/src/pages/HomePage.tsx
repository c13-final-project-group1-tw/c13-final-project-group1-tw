import React from 'react'
import GroupButtonHome from '../components/GroupButtonHome';
import Logo from '../components/Logo';
import modules from '../css/HomePage.module.css';


function HomePage() {
    const onScroll=((e:any) =>{
        console.log(e.currentTarget.scrollTop, e.currentTarget.scrollHeight);
    })
    return (
        <div onScroll={onScroll} className={modules.container}>
            <Logo />
            
            <GroupButtonHome />
        </div>
    )
}
//<InfListTest />
export default HomePage;
