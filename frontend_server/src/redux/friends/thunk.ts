import { Dispatch } from "redux";
import { IRootState } from "../store";
import { IProductActions, resetProductList, setProductList } from "./action";

const { REACT_APP_API_SERVER } = process.env;

export function getFriendListThunk(pageNum:number, isInit: boolean){
    return async (dispatch:Dispatch<IProductActions>, getState:()=> IRootState)=>{
        try{
            if(isInit){
                dispatch(resetProductList());
            }

            const res = await fetch(`${REACT_APP_API_SERVER}/getAllProduct/${pageNum}`);
            if(res.status === 200){
                const data = await res.json();
                console.log(data);
                if (pageNum !== 1) {
                    const newData = getState().products.productList.concat(data.data);
                    dispatch(setProductList(newData, data.activePage, data.nextPage, data.limit));
                    return;
                }
                dispatch(setProductList(data.data, data.activePage, data.nextPage, data.limit));
                return;
            }
            // handle
        }catch(err){
            console.log(err);
            // handle
        }
        }
    
}

export function getProductListByClassThunk(pageNum:number, pClass:string , isInit: boolean){
    return async (dispatch:Dispatch<IProductActions>, getState:()=> IRootState)=>{
        try{
            if(isInit){
                dispatch(resetProductList());
            }
            const res = await fetch(`${REACT_APP_API_SERVER}/getAllProduct/${pClass}/${pageNum}`);
            if(res.status === 200){
                const data = await res.json();
                console.log(data);
                if (pageNum !== 1) {
                    const newData = getState().products.productList.concat(data.data);
                    dispatch(setProductList(newData, data.activePage, data.nextPage, data.limit));
                    return;
                }
                dispatch(setProductList(data.data, data.activePage, data.nextPage, data.limit));
                return;
            }
            // handle
        }catch(err){
            console.log(err);
            // handle
        }
        }
    
}