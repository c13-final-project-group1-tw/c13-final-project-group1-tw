export interface IProduct {
    id: number;
    product_id_code: number;
    product_name: string;
    brand_name: number;
    product_url: string;
    product_pic: string;
    sub_category_name: number;
    category_name: number;
    gender: string;
    original_price: number;
    color_name: string;
}

export interface IProductState {
    productList: Array<IProduct>;
    activePage: number;
    nextPage: number;
    limit: boolean;
    msg: string;
}

export const initProductState: IProductState ={
    productList: [],
    activePage: 1,
    nextPage: 2,
    limit: false,
    msg: '',
}