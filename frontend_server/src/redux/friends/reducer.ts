import { IProductState, initProductState} from './state';
import { IProductActions } from './action';

export const productReducer = (state: IProductState = initProductState, action:IProductActions):IProductState=> {
    
    switch (action.type){
        case "@@FRIENDS/SET_FRIEND_LIST":
            return{
                ...state,
                productList: action.products,
                activePage: action.activePage,
                nextPage: action.nextPage,
                limit: action.limit,
            };
        case "@@FRIENDS/RESET":
            return{
                ...state,
                productList: initProductState.productList,
            }
        default:
            return state;
    }
}