import {createStore, combineReducers, compose, applyMiddleware} from "redux";
import {
    RouterState,
    connectRouter,
    routerMiddleware,
    CallHistoryMethodAction
  } from 'connected-react-router';
import { createBrowserHistory } from "history";


// auth , router
import { IAuthState } from "./auth/state";
import {authReducer} from "./auth/reducer";
import logger from "redux-logger";
import { IAuthAction } from './auth/action';
import thunk,{ThunkDispatch as OldThunkDispatch} from 'redux-thunk';

import {IProductState} from '../redux/friends/state';
import {productReducer} from '../redux/friends/reducer';
import { IProductActions } from "./friends/action";

export const history = createBrowserHistory();
// IRootState
export interface IRootState {
    auth: IAuthState;
    products: IProductState;
    router: RouterState;
}
// initState
type IRootAction = IAuthAction | IProductActions |　CallHistoryMethodAction;

// Thunk Dispatch
export type ThunkDispatch = OldThunkDispatch<IRootState, null, IRootAction>;

// createStore
const rootReducer = combineReducers<IRootState>({
    auth: authReducer,
    products: productReducer,
    router: connectRouter(history),
});

declare global{
    /* tslint:disable:interface-name */
    interface Window{
        __REDUX_DEVTOOLS_EXTENSION_COMPOSE__:any
    }
}

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

// createStore
export default createStore<IRootState,IRootAction,{},{}>(
    rootReducer,
    composeEnhancers(applyMiddleware(logger), applyMiddleware(thunk), applyMiddleware(routerMiddleware(history))
));