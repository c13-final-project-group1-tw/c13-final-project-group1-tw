export interface IUsers {
    id: number;
    users_is_active: boolean;
    seller_is_active: boolean;
    customer_first_name: string;
    customer_last_name: string;
    password: string;
    username: string;
    email: string;
    location: string;
    gender: string;
    is_active: boolean;
    photo_str: number;
    pending:string;
    brand_id: number;
}

// step 1 : Interface
export interface IAuthState {
    userResultList: IUsers | null;
    isAuthenticated: boolean | null;
}
// step 2 : InitState
export const initAuthState: IAuthState = {
    userResultList: null,
    isAuthenticated: null, // false
};

