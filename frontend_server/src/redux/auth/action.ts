import { CallHistoryMethodAction } from "connected-react-router";
import {IUsers} from './state';

// action creator
export function loginSuccess(){
    return{
        type: "@@AUTH/LOGIN_SUCCESS" as const,
    };
}

export function loginFail(){
    return{
        type: "@@AUTH/LOGIN_FAIL" as const,
    };
}

export function logout(){
    return{
        type: "@@AUTH/LOGOUT" as const,
    };
}

export function register(){
    return{
        type: "@@AUTH/REGISTER" as const,
    };
}

export function getUser(usersList:IUsers){
    return{
        type: "@@AUTH/GETUSER" as const,
        usersList,
    };
}

export function updateUser(){
    return{
        type: "@@AUTH/UPDATEUSER" as const,
    };
}

type authActionCreators = typeof loginSuccess | typeof loginFail | typeof logout | typeof register | typeof getUser | typeof updateUser;

export type IAuthAction = ReturnType<authActionCreators> | 　CallHistoryMethodAction;