import {IAuthState, initAuthState} from "./state";
import {IAuthAction} from './action';

export const authReducer = (state: IAuthState = initAuthState, action: IAuthAction): IAuthState =>{
    switch(action.type){
        case "@@AUTH/LOGIN_SUCCESS":
            return{
                ...state,
                isAuthenticated: true,
            };
        
        case "@@AUTH/LOGIN_FAIL":
            return{
                ...state,
                isAuthenticated: false,
            };
        
        case "@@AUTH/LOGOUT":
            return{
                ...state,
                isAuthenticated: false,
            };
        
        case "@@AUTH/REGISTER":
            return{
                ...state,
                isAuthenticated: true,
            };
        
        case "@@AUTH/GETUSER":
            return{
                ...state,
                isAuthenticated: true,
                userResultList: action.usersList,
            };
        
        case "@@AUTH/UPDATEUSER":
            return{
                ...state,
                isAuthenticated: true,
            };

        default:
            return state;
    }
};