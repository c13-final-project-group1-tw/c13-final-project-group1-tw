import { Dispatch } from 'redux';
import { getUser, IAuthAction, loginFail, loginSuccess, register } from './action';
import { push } from 'connected-react-router';

const { REACT_APP_API_SERVER } = process.env;

export function restoreLoginThunk(){
    return async(dispatch:Dispatch<IAuthAction>)=>{
        const token = localStorage.getItem("token");
        if(!token){
            dispatch(loginFail());
            return;
        }
        const res = await fetch(`${REACT_APP_API_SERVER}/users/validate`,{
            headers: {
                "Authorization":`Bearer ${token}`,
            },
        });
        if(res.status !== 200){
            dispatch(loginFail());
            return;
        }
        dispatch(loginSuccess());
        return;
    };
}

export function loginThunk(email:string, password:string){
    return async (dispatch:Dispatch<IAuthAction>)=>{
        const res = await fetch(`${REACT_APP_API_SERVER}/customerLogin`, {
            method: "POST",
            headers: {
                "Content-Type": "application/json",
            },
            body: JSON.stringify({email, password}),
        });
        const result = await res.json();

        if(res.status === 200){
            localStorage.setItem("token", result.token);
            dispatch(loginSuccess());
            dispatch(push('/home'));
        }else{
            dispatch(loginFail());
        }
    }
}
export function registerThunk(data: any){
    return async (dispatch:Dispatch<IAuthAction>)=>{
        const res = await fetch(`${REACT_APP_API_SERVER}/customerRegister`,{
            method: "POST",
            headers: {
                "Content-Type": "application/json",
            },
            body: JSON.stringify(data),
        });
        const result = await res.json();
        if(res.status === 200){
            dispatch(register());
            loginThunk(data.email,data.password);
        }else{   
            console.log(result.message);
            dispatch(loginFail());
        }
    }
}

export function getUserThunk(){
    return async(dispatch:Dispatch<IAuthAction>)=>{
        try {
            const token = localStorage.getItem("token");
            if(!token){
                dispatch(loginFail());
                return;
            }
            const res = await fetch(`${REACT_APP_API_SERVER}/profile/Customer`,{
                method: 'GET',
                headers: {
                    "Authorization":`Bearer ${token}`,
                },
            });

            const result = await res.json();
            
            if(res.status === 200){
                dispatch(getUser(result.data));
            }else{
                dispatch(getUser(result.data));
                alert('You have some invalid input.');
            }
        } catch (err) {
            console.log(err.message);
        }
        

    }
}