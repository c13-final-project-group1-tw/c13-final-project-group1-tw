import React, { useState } from 'react';
import { Collapse, Navbar, NavbarToggler, Nav, NavItem, NavLink } from 'reactstrap';
import LoginModalBody from './LoginModalBody';
import { useDispatch, useSelector } from 'react-redux';
import { IRootState } from '../redux/store';
import { push } from 'connected-react-router';
import { logout } from '../redux/auth/action';
import modules from '../css/NavBarComp.module.css';

function NavBarComp() {
    const dispatch = useDispatch();
    const [collapsed, setCollapsed] = useState(true);
    const toggleNavbar = () => setCollapsed(!collapsed); // default false
    const isAuthenticated = useSelector((state: IRootState)=> state.auth.isAuthenticated);
    let navBarState;
    let logoHref = '/';

    const profileClick = () => {
        toggleNavbar();
        dispatch(push('/userprofile'));
    };
    const logoutClick = () => {
        toggleNavbar();
        dispatch(logout());
        localStorage.removeItem("token");
        dispatch(push(logoHref));
    }
    
    const ProductAllClick = () =>{
        toggleNavbar();
        dispatch(push('/product/All'));
    }
    const HomeClick = ()=>{
        toggleNavbar();
        dispatch(push(logoHref));
    }

    if(isAuthenticated){
        logoHref = '/home';
        navBarState = 
            <div>
                <NavItem>
                   <NavLink onClick={HomeClick}>Home</NavLink>
                </NavItem>
                <NavItem>
                   <NavLink onClick={profileClick}>Profile</NavLink>
                </NavItem>
                <NavItem>
                    <NavLink onClick={ProductAllClick}>All Product</NavLink>
                </NavItem>
                <NavItem>
                    <NavLink href="mailto:contact-us@shopround.co">Contact us</NavLink>
                </NavItem>
                <NavItem>
                    <NavLink onClick={logoutClick}>Logout</NavLink>
                </NavItem>
            </div>
        
    }else{
        logoHref = '/';
        navBarState = 
            <div>
                <NavItem>
                   <NavLink onClick={HomeClick}>Home</NavLink>
                </NavItem>
                <NavItem onClick={()=>toggleNavbar()}>
                    <LoginModalBody />
                </NavItem>
                <NavItem>
                    <NavLink onClick={ProductAllClick}>All Product</NavLink>
                </NavItem>
                <NavItem onClick={() => toggleNavbar()}>
                    <NavLink href="mailto:contact-us@shoppound.co">Contact us</NavLink>
                </NavItem>
            </div>
    }
    return (
        <div>
            <Navbar className={modules.stickyNav} color="faded" light>
                
                <NavbarToggler onClick={toggleNavbar} className="mr-2" />
                <Collapse isOpen={!collapsed} navbar>
                <Nav navbar>
                    {navBarState}
                </Nav>
                </Collapse>
            </Navbar>
        </div>
    )
}

export default NavBarComp;

