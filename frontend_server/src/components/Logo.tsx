import React from 'react';
import logo from '../img/logo_design.png';

function Logo() {
    return (
        <div style={{display: 'flex',justifyContent: 'center', alignItems: 'center'}}>
            <img src={logo} alt="logo" style={{maxWidth:'60%'}}/>
        </div>
    )
}

export default Logo;
