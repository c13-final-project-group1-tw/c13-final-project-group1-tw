import React from 'react';
import { Container, UncontrolledCarousel } from 'reactstrap';
import modules from '../css/LandPageCarousel.module.css';
import { IRootState } from '../redux/store';
import { useSelector } from 'react-redux';

function LandPageCarousel() {
    const items = [
        {
          src: 'https://shoplineimg.com/56cb124503905552820000df/5f55bd089076ca002dd865e0/1080x.jpg?',
          altText: 'Slide 1',
          caption: 'Slide 1',
          header: 'Slide 1 Header',
          key: '1'
        },
        {
          src: 'https://shoplineimg.com/56cb124503905552820000df/5edf42e45aab2e0024f59b1f/1080x.jpg?',
          altText: 'Slide 2',
          caption: 'Slide 2',
          header: 'Slide 2 Header',
          key: '2'
        },
        {
          src: 'https://shoplineimg.com/56cb124503905552820000df/5fad125fe7414b00230d7719/1080x.jpg?',
          altText: 'Slide 3',
          caption: 'Slide 3',
          header: 'Slide 3 Header',
          key: '3'
        },
        {
            src: 'https://shoplineimg.com/5caf12ad9f29f000017a99f0/604edde111c81a00295514c4/800x.webp?source_format=jpg',
            altText: 'Slide 4',
            caption: 'Slide 4_2',
            header: 'Slide 4 Header',
            key: '4'
          }
      ];
      const friendList = useSelector((state:IRootState) => state.products.productList);
      console.log(friendList);
    return (
      <Container className={modules.container}>
          <UncontrolledCarousel items={items} />
        </Container>
    )
}

export default LandPageCarousel;
