import React from 'react';
import Image from 'react-bootstrap/Image'
import upArrow from '../img/upArrow.jpg';

function GoToTopButton() {
    const scrollToTop = ()=>{
        window.scrollTo(0, 0);
    }
    return (
        <div style={{position: 'fixed', right: '1%', bottom: '1%'}} onClick={scrollToTop}>
            <Image src={upArrow} alt="Top" roundedCircle style={{border: '1px solid rgba(0,0,0,0.125)',height:'40px', width:'40px'}}></Image>
        </div>
    )
}

export default GoToTopButton;
