export interface Users {
  id: number;
  users_is_active: boolean;
  seller_is_active: boolean;
  customer_first_name: string;
  customer_last_name: string;
  password: string;
  username: string;
  email: string;
  location: string;
  gender: string;
  is_active: boolean;
  photo_str: number;
  pending:string;
  brand_id: number;
}// user join customer, seller, photo

export interface Customer {
  id: number;
  customer_first_name: string;
  customer_last_name: string;
  password: string;
  username: string;
  email: string;
  location: string;
  gender: string;
  preferred_category: string;
  photo: number;
  facebook_auth: number;
  google_auth: number;
  apple_auth: number;
  is_active: boolean;
}

export interface Brand {
  id: number;
  brand_name: string;
  brand_pic: string;
  is_active: boolean;
}

export interface Category {
  id: number;
  category_name: string;
}

export interface SubCategory {
  id: number;
  sub_category_name: string;
  category_id: number;
}

export interface CustomerPrefer {
  id: number;
  customer_id: number;
  click_prefer: number;
}

export interface Product {
  id: number;
  product_name: string;
  brand_id: number;
  product_url: string;
  price: number;
  origin_price: number;
  product_pic: string;
  product_subcategory_id: number;
  is_active: boolean;
}

export interface CustomerFavourite {
  id: number;
  customer_id: number;
  brand_id: number;
  product_id: number;
  string_favourite: Text;
}

export interface Color {
  id: number;
  color_name: string;
}

export interface Price {
  id: number;
  sale_price: number;
  original_number: number;
  product_id: number;
}

export interface Session {
  id: number;
  customer_id: number;
  product_id: number;
  event: string;
  session_code: string;
}
