/*
import { Request, Response, NextFunction } from 'express';

export function isLoggedIn(req: Request, res: Response, next: NextFunction) {
  if (req.session?.['user']) {
    next();
  } else {
    res.redirect('/index.html', 401);
  }
} // todo checking?
*/
import {Bearer} from 'permit';
import jwtSimple from 'jwt-simple';
import express from 'express';
import jwt from './jwt';
import {knex} from '../main';
//import './model';

const permit = new Bearer({
    query:"access_token"
});

export interface Users{
    id:number
    email:string
    password:string
}


declare global{
    namespace Express{
        interface Request{
            user?: {
                id: number;
                username: string;
            }
        }
    }
}

export async function isLoggedIn(
    req:express.Request,
    res:express.Response,
    next:express.NextFunction){
try{
const token = permit.check(req); // check token
if(!token){
return res.status(401).json({msg:"Permission Denied"});
}
const payload = jwtSimple.decode(token,jwt.jwtSecret);
//const user:User = await userService.getUser(payload.id);
const user = await knex("users").where("id", payload.id).first();
if(user){
    const {password, ...others} = user;
    req.user = {...others};
    return next();
} else {
    return res.status(401).json({msg:"Permission Denied 1"});
}
    }catch(e){
        console.log(e.message)
        return res.status(401).json({msg:"Permission Denied 2"});
    }
}