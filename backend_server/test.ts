import Knex from 'knex';
import { tables } from './utils/tables';
import * as knexConfig from './knexfile';
import { Customer, Product, Session } from './utils/model';
import { EVENT_WEIGHT } from './utils/weights';

type CustomerID = number;
type Product_id = number;
// type SimMatrix = Map<UserID, Map<UserID, number>>;

function cloneUIMatrix(uiMatrix: Map<CustomerID, Map<Product_id, number>>) {
  const resultMatrix = new Map<CustomerID, Map<Product_id, number>>();
  uiMatrix.forEach((row, customerID) => {
    resultMatrix.set(customerID, new Map<Product_id, number>());
    row.forEach((cell, Product_id) => {
      resultMatrix.get(customerID)?.set(Product_id, cell);
    });
  });
  return resultMatrix;
}

function findTopKUsers(simMatrix: Map<CustomerID, Map<CustomerID, number>>, customerID: number, K = 2) {
  const scores: Array<{ uid: number; score: number }> = [];
  simMatrix.get(customerID)?.forEach((score, uid) => {
    if (uid !== customerID) {
      scores.push({ uid, score });
    }
  });
  return scores
    .sort((a, b) => b.score - a.score)
    .slice(0, K)
    .map((score) => score.uid);
}

async function main() {
  const knex = Knex(knexConfig['development']);
  try {
    const uiMatrix = new Map<CustomerID, Map<Product_id, number>>();

    const customers = await knex<Customer>(tables.CUSTOMER);
    const products = await knex<Product>(tables.PRODUCT);
    const customerIDs = customers.map((customer) => customer.id);
    const Product_ids = products.map((product) => product.id);

    // create an UI Matrix with NaN values
    for (const customer of customers) {
      const productMap = new Map<Product_id, number>();
      for (const product of products) {
        productMap.set(product.id, 0);
      }
      uiMatrix.set(customer.id, productMap);
    }

    // calculate scores in the UI Matrix
    const sessions = await knex<Session>(tables.CUSTOMER_SESSION);
    let min = Number.MAX_SAFE_INTEGER;
    let max = Number.MIN_SAFE_INTEGER;
    for (const session of sessions) {
      const weight: number = EVENT_WEIGHT[session.event];
      const currentScore = uiMatrix.get(session.customer_id)?.get(session.product_id) as number;
      const updatedScore = currentScore + weight;
      if (updatedScore < min) {
        min = updatedScore;
      }
      if (updatedScore > max) {
        max = updatedScore;
      }
      uiMatrix.get(session.customer_id)?.set(session.product_id, updatedScore);
    }

    // normalize the UI Matrix
    uiMatrix.forEach((productMap) => {
      productMap.forEach((score, key) => {
        if (score !== 0) {
          const updatedScore = ((score - min) / (max - min)) * 9 + 1;
          productMap.set(key, updatedScore);
        }
      });
    });
    // console.log(uiMatrix);

    // create an similarity Matrix
    let simMatrix = new Map<CustomerID, Map<CustomerID, number>>();
    for (let i = 0; i < customers.length; i++) {
      const customerMap = new Map<CustomerID, number>();
      for (let j = 0; j < customers.length; j++) {
        customerMap.set(customers[j].id, NaN);
      }
      simMatrix.set(customers[i].id, customerMap);
    }
    //console.log(simMatrix);

    // calculate productMeanMap
    const normUIMatrixWithproductMean = cloneUIMatrix(uiMatrix);
    const productMeanMap = new Map<CustomerID, number>();
    normUIMatrixWithproductMean.forEach((row, customerID) => {
      let mean = 0;
      let counter = 0;
      row.forEach((cell) => {
        if (cell !== 0) {
          counter++;
          mean += cell;
        }
      });
      mean /= counter;
      productMeanMap.set(customerID, mean);
    });
    // console.log(productMeanMap);

    // Normalize the scores in UI Matrix with product means
    normUIMatrixWithproductMean.forEach((row, customerID) => {
      row.forEach((cell, Product_id) => {
        if (cell !== 0) {
          const updatedValue = cell - (productMeanMap.get(customerID) as number);
          row.set(Product_id, updatedValue);
        }
      });
    });
    console.log(uiMatrix);

    // calculate similarity Matrix
    for (let i = 0; i < customerIDs.length; i++) {
      for (let j = i + 1; j < customerIDs.length; j++) {
        const xUserID = customerIDs[i];
        const yUserID = customerIDs[j];
        console.log(`[info] calculate similarity of customers: [${xUserID}], [${yUserID}]`);

        const xUserRow = normUIMatrixWithproductMean.get(xUserID);
        const yUserRow = normUIMatrixWithproductMean.get(yUserID);

        let covariance = 0;
        let sX = 0;
        let sY = 0;
        for (const Product_id of Product_ids) {
          const cellX = xUserRow?.get(Product_id) as number;
          const cellY = yUserRow?.get(Product_id) as number;
          covariance += cellX * cellY;
          sX += cellX * cellX;
          sY += cellY * cellY;
        }
        const result = covariance / (Math.sqrt(sX * sY) + 0.001);
        console.log(`[info] similarity of customers: [${xUserID}, ${yUserID}] = [${result}]`);
        simMatrix.get(xUserID)?.set(yUserID, result);
        simMatrix.get(yUserID)?.set(xUserID, result);
      }
    }
    // console.log(simMatrix);

    // loop over UI Matrix and calculate new score
    uiMatrix.forEach((row, customerID) => {
      row.forEach((cell, Product_id) => {
        if (cell === 0) {
          console.log(`[info] calculating score for customer: [${customerID}], product: [${Product_id}]`);
          const topKUserIDs = findTopKUsers(simMatrix, customerID, 2);
          let sum = 0;
          let counter = 0;
          for (const simUserID of topKUserIDs) {
            const score = uiMatrix.get(simUserID)?.get(Product_id) as number;
            if (isNaN(score)) {
              console.log(customerID, Product_id, sum);
            }
            if (score !== 0) {
              sum += score;
              counter++;
            }
          }
          let newScore = 1;
          if (counter !== 0) {
            newScore = sum / counter;
          }
          console.log(`[info] score for customer: [${customerID}], product: [${Product_id}] = [${newScore}]`);
          uiMatrix.get(customerID)?.set(Product_id, newScore);
        }
      });
    });
    // console.log(uiMatrix);

    // insert into DB
    const toBeInsertRatings: Array<{
      customer_id: number;
      product_id: number;
      rating: number;
    }> = [];
    uiMatrix.forEach((row, customerID) => {
      row.forEach((cell, Product_id) => {
        toBeInsertRatings.push({
          customer_id: customerID,
          product_id: Product_id,
          rating: Number(cell.toFixed(4)),
        });
      });
    });
    console.log('[info] toBeInsertRatings: ');
    console.log(toBeInsertRatings);
    await knex(tables.RATING).del();
    await knex(tables.RATING).insert(toBeInsertRatings);
  } catch (err) {
    console.error(err.message);
  } finally {
    await knex.destroy();
  }
}

main();
