import express from 'express';
import { userController } from '../../main';
import { isLoggedIn } from '../../utils/guards';

export const userRoutes = express.Router();

userRoutes.post('/customerRegister', userController.createCustomer);
userRoutes.post('/customerLogin', userController.customerLogin);
userRoutes.get('/logout', isLoggedIn, userController.logout);
userRoutes.get('/profile/Customer', isLoggedIn, userController.getCustomer);
userRoutes.put('/profile/Customer'/**userController.updateCustomer */);
userRoutes.get('/getCustomerForUpdate', isLoggedIn, userController.getCustomerForUpdate);
userRoutes.post('/updateCustomerWithPw', isLoggedIn, userController.updateCustomerWithPw);
userRoutes.post('/updateCustomerWithNoPw', isLoggedIn, userController.updateCustomerWithNoPw);
userRoutes.get("/users/validate", isLoggedIn, userController.validate);
// add by future
