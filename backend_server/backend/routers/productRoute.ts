import express from 'express';
import { productController } from '../../main';
import { isLoggedIn } from '../../utils/guards';

export const productRoutes = express.Router();

// productRoutes.post('/addProduct', productController.createProduct);
// productRoutes.post('/updateProduct', productController.updateProduct);
productRoutes.get('/getAllProduct/:pageNum', productController.getAllProduct);
productRoutes.get('/getAllBrand', productController.getBrandName);
productRoutes.get('/getRatingProduct', isLoggedIn, productController.getRatingProduct);
productRoutes.get('/getAllProduct/:pClass/:pageNum', productController.getProductByClass);
//may change
