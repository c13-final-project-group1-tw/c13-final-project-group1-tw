import * as Knex from 'knex';
import { Brand } from '../../utils/model';
import { tables } from '../../utils/tables';

export class ProductService {
  constructor(private knex: Knex) {}

  async getAllProduct(offset:number, limit:number):Promise<any> {
    return (await this.knex.select('product.id','product.product_id_code','product.product_name','brand.brand_name','product.product_url','product.product_pic','sub_category.sub_category_name','category.category_name','product.gender', 'price.original_price', 'color.color_name')
    .from(tables.PRODUCT)
    .leftJoin('brand', 'brand.id', 'product.brand_id')
    .leftJoin('color', 'color.product_id', 'product.id')
    .leftJoin('price', 'price.product_id', 'product.id')
    .leftJoin('sub_category', 'sub_category.id', 'product.subcategory_id')
    .leftJoin('category', 'category.id', 'product.category_id')
    .limit(limit).offset(offset));
  } // get all product in public index.html

  async getProductByClass(offset:number, limit:number, pClasses:string):Promise<any> {
    return (await this.knex.select('product.id','product.product_id_code','product.product_name','brand.brand_name','product.product_url','product.product_pic','sub_category.sub_category_name','category.category_name','product.gender', 'price.original_price', 'color.color_name')
    .from(tables.PRODUCT)
    .where('category.category_name', `${pClasses}`)
    .leftJoin('brand', 'brand.id', 'product.brand_id')
    .leftJoin('color', 'color.product_id', 'product.id')
    .leftJoin('price', 'price.product_id', 'product.id')
    .leftJoin('sub_category', 'sub_category.id', 'product.subcategory_id')
    .leftJoin('category', 'category.id', 'product.category_id')
    .limit(limit).offset(offset));
  }

  async getAllBrand(): Promise<Brand> {
    return await this.knex.select('*').from(tables.BRAND);
  } // take brand to filter

  async getRatingProduct(customerID: number) {
    return await this.knex
      .select('*')
      .from('product')
      .leftJoin('rating', 'product.id', 'rating.product_id')
      .where('rating.customer_id', customerID)
      .orderBy('rating.rating', 'desc');
  } // select * from product left join rating on product.id = rating.product_id where rating.customer_id = $1 order by rating.rating desc;
  // get all rating product in private index.html

  async checkBrand(brandName: string) {
    return await this.knex(tables.BRAND).count('*').where('brand_name', brandName);
  }

  async checkColor(colorName: string) {
    return await this.knex(tables.COLOR).count('*').where('color_name', colorName);
  }

  async checkCategory(categoryName: string) {
    return await this.knex(tables.CATEGORY).count('*').where('category_name', categoryName);
  }

  async checkSubCategory(subCategoryName: string) {
    return await this.knex(tables.SUB_CATEGORY).count('*').where('sub_category_name', subCategoryName);
  }
}
