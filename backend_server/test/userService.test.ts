import Knex from 'knex';
const knexfile = require('../knexfile');
const knex = Knex(knexfile['test']);
import { UserService } from '../backend/service/UserService';
// import {UserController} from '../controller/UserController';
// import { Page, chromium } from 'playwright';
// import '../main';

describe.skip('UserService Testing', () => {
  let userService: UserService;
  //let userController:UserController;
  beforeAll(async () => {
    await knex.migrate.down();
    await knex.migrate.latest();
  });
  beforeEach(async () => {
    //userController = new User
    userService = new UserService(knex);
    await knex('customer').del();
    await knex
      .insert([
        {
          email: 'Peter@gmail.com',
          password: '123',
        },
        { email: 'Ben@gmail.com', password: '456' },
      ])
      .into('customer')
      .returning('id');
  });

  it.skip('should get all students', async () => {
    const customers = await userService.getAllCustomer();
    expect(customers[0].email).toBe('Peter@gmail.com');
    expect(customers[1].email).toBe('Ben@gmail.com');
  });

  afterAll(() => {
    knex.destroy();
  });

  //it.skip('should display "Login" page and text on title', async()=>{
  //    await page.goto('http://localhost:8080/');
  //    const title = await page.title();
  //    expect(title).toContain('Login');
  //});
});
