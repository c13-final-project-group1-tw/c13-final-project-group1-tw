import Knex from 'knex';
const knexfile = require('../knexfile');
const knex = Knex(knexfile['test']);
import { Page, chromium } from 'playwright';
import '../main';

describe('Login', () => {
  let page: Page;
  beforeAll(async () => {
    const browser = await chromium.launch();
    page = await browser.newPage();
  });

  it.only('should be successfully login', async () => {
    await page.goto('http://localhost:8080/');
    await page.evaluate(() => {
      const username = document.querySelector('[name=email]');
      const password = document.querySelector('[name=password]');
      if (username && password) {
        (username as HTMLInputElement).value = 'peter@gmail.com';
        (password as HTMLInputElement).value = '123';
      }
      const submit = document.querySelector('[type=submit]');
      if (submit) {
        (submit as HTMLInputElement).click();
      }
    });
    const LoginMain = await page.evaluate(() => document.querySelector('#logout'));
    expect(LoginMain).toBeDefined();
  });

  it.skip('should display "Login" page and text on title', async () => {
    await page.goto('http://localhost:8080/');
    const title = await page.title();
    expect(title).toContain('Login');
  });
  afterAll(() => {
    knex.destroy();
  });
});
