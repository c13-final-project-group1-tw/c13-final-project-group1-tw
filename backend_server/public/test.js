window.onload = () => {
    initLoginForm()
    loadAllProduct()
}

function initLoginForm() {
    try {
        const loginForm = document.getElementById('loginForm')

        loginForm.addEventListener('submit', async (event) => {
            event.preventDefault()
            const formObject = {}
            formObject['email'] = loginForm.email.value
            formObject['password'] = loginForm.password.value
            //console.log(formObject);
            const res = await fetch('/customerLogin', {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json; charset=utf-8',
                },
                body: JSON.stringify(formObject),
            })

            if (res.status === 200) {
                const data = await res.json()
                window.alert('Success login, welcome!')
                let location = '/login_index.html'
                window.location = location
            } else {
                const data = await res.json()
                alert(data.message)
            }
        })
    } catch (err) {
        //console.log(err.message);
    }
}

async function loadAllProduct() {
    try {
        const res = await fetch('/getAllProduct')
        const data = await res.json()
        console.log(data.data)
        let productList = ``
        let count = 1
        for (const currList of data.data) {
            productList += `
            <div class="card" style="width: 18rem;">
                <img src="${currList.product_url}" class="card-img-top" alt="...">
                <div class="card-body">
                <p class="card-text">${currList.id} ${currList.product_name}</p>
                </div>
            </div>
            `
            count += 1
        }
        document.querySelector('#productList').innerHTML = productList
    } catch (err) {
        console.log(err.message)
    }
}
