from os import error, sep, write
import requests
from bs4 import BeautifulSoup
import json
import re
from selenium import webdriver
import csv

data={}
data["cloth"] = []

def process(url):
    #print("[info] processing url: [{}]".format(url))
    headers = {
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Methods': 'GET',
        'Access-Control-Allow-Headers': 'Content-Type',
        'Access-Control-Max-Age': '3600',
        'User-Agent': 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:52.0) Gecko/20100101 Firefox/52.0'
    }

    driver = webdriver.Chrome("./chromedriver")
    #time.sleep(10)
    driver.implicitly_wait(5)
    if url is not None:
        driver.get(url)
        #print(driver.title)
        #html=driver.page_source
        #print(html)
        driver.close()
        driver.quit()

        req = requests.get(url, headers)
        req.encoding= "utf8"
        soup = BeautifulSoup(req.content, 'html.parser')
        priceDiv = soup.find("span", class_="money")
        #print(priceDiv)
        prod_detail = soup.find()
        #print(prod_detail)
        fp = open("index.html", "w", encoding="utf8")
        fp.write(soup.prettify())
        titleH1 = soup.find("h1", class_="ProductMeta__Title")
        #print(titleH1)
        productDes = soup.find("div", class_="Rte")
        product_images = soup.find_all("img", class_="Image--lazyLoad");
        SizeSwatches= soup.find_all("label", class_="SizeSwatch");

        linkStr = ""
        #print(product_images)
        for product_image in product_images:
            # print(product_image)
            link = product_image.get("data-original-src")
            linkStr += "https:"+link + ', '
            #print(link)
            price = (int(re.sub(r"[\n\t\s\$\,]*", "",priceDiv.string))*7.78)
            #print(price)
        
        linkString = ""
        for SizeSwatche in SizeSwatches:
            linkage = SizeSwatche.get_text()
            #print(linkage)]
            if(linkage==''):
                linkage = 'F'
            linkString += linkage +','


        SColors=soup.find("li", string=re.compile("Color:"))
        if (SColors != None):
            #print(SColors.text)
            Colors = SColors.text
            Colors = Colors.replace("Color:"," ")
            #print(Colors)

        if (SColors==None):
            SColors=soup.find("li", string=re.compile("Colour –"))
            if (SColors!=None):
                Colors = SColors.text
                Colors = Colors.replace("Colour –"," ")
            
        if (SColors == None):
            SColors = soup.find("li", string=re.compile("Colours:"))
            if (SColors!=None):
                Colors = SColors.text
                Colors = Colors.replace("Colours:"," ")

        if (SColors==None):
            Colors = "Unknown"

        
        #print(Colors)
        #print(html)

        ScrapMaterial=soup.find("li", string=re.compile("Viscose"))
        if(ScrapMaterial!=None):
            Material = ScrapMaterial.text
        
        if(ScrapMaterial==None):
            ScrapMaterial=soup.find("li", string=re.compile("100%"))
            if(ScrapMaterial!=None):
                Material = ScrapMaterial.text
        
        if(ScrapMaterial==None):
            ScrapMaterial=soup.find("li", string=re.compile("Silk"))
            if(ScrapMaterial!=None):
                Material = ScrapMaterial.text
        
        if(ScrapMaterial==None):
            ScrapMaterial=soup.find("li", string=re.compile("Fabric"))
            if(ScrapMaterial!=None):
                Material = ScrapMaterial.text

        if(ScrapMaterial==None):
                Material = "Unknown"

        #print(Material)

        t = productDes.text

        t1 = t.find("顔色：")
        t2 = t.find("特點：")
        t3 = t.find("尺寸：")
        t4 = t.find("產品：")
        t5 = t.find("材質：")

        color=(t[t1+3:t2].strip())
        size=(t[t3+3:t1].strip())
        mat=(t[t5+3:t3].strip())

        #print(color)
        #print(size)
        #print(mat)

        fp.write(productDes.prettify())
        
        data["cloth"].append({
            "name": titleH1.string,
            "price": price,
            "photo": linkStr,
            "color": Colors,
            "size": linkStr,
            "material": Material, 
            "from" : "https://tablatm.com/",
            "brand" :"tablatm",
            "url":"url"
        })

        fp.write(productDes.prettify())
        fp.write(titleH1.prettify())
        fp.write(priceDiv.prettify())
        fp.write(productDes.prettify())
        fp.write(Colors)
        fp.write(linkString)
        fp.write(Material)
        #fp.write(SizeSwatche.prettify())
        #print(productDes)
        #print("Writing index.html")

    csvfile = "SpTaniaData.csv"
    list1=[titleH1.string,price,linkStr,Colors,linkString,Material,"https://tablatm.com/","tablatm",url]
    with open(csvfile, 'a', newline='')as fp:
        writer=csv.writer(fp)
        writer.writerow(list1)
        fp.close()

if __name__== '__main__': 
    csvfile = "SpTaniaData.csv"
    with open(csvfile, 'a', newline='')as fp:
        writer=csv.writer(fp)
        writer.writerow(["name","price","photo","color","size","material","from","brand","url"])
        fp.close()
    print("[info] start processing...")
    with open("SpTania.txt") as f:
        try:
            for url in f.readlines():
                url = url.strip()
                process(url)
        except (RuntimeError,TimeoutError,TypeError):
            pass
            print("Oops! there is RuntimeError, TimeoutError & TypeError. Try again")
        except(OSError):
            pass 
            print("Oops!It have OSError")